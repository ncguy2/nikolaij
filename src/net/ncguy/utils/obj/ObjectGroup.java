package net.ncguy.utils.obj;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nick on 18/01/2015 at 12:03.
 * Project: Development
 * Package: net.ncguy.utils.obj
 */
public class ObjectGroup<T> {

    public List<T> instances;

    public ObjectGroup() {
        instances = new ArrayList<>();
    }

    public List<T> getItems() {
        return instances;
    }

}
