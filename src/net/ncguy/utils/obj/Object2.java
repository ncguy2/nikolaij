package net.ncguy.utils.obj;

/**
 * Created by ncguy on 04/12/2014 at 13:05.
 */
public class Object2<T> {

    public T x, y;

    public Object2() {}

    public Object2(T x, T y) {
        this.x = x;
        this.y = y;
    }
}
