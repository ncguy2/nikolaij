package net.ncguy.utils.obj;

/**
 * Created by ncguy on 04/12/2014 at 13:07.
 */
public class Object3<T> {

    public T x, y, z;

    public Object3() {}

    public Object3(T x, T y, T z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

}
