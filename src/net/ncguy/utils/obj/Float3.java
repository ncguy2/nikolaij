package net.ncguy.utils.obj;

/**
 * Created by ncguy on 08/12/2014 at 17:48,
 * Project: Development.
 */
public class Float3 {

    public float a, b, c;

    public Float3() { a = b = c = 0; }
    public Float3(float a, float b, float c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }


}
