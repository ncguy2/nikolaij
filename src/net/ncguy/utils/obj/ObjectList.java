package net.ncguy.utils.obj;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nick on 28/12/2014 at 23:06.
 * Project: Development
 * Package: net.ncguy.utils.obj
 */
public class ObjectList<T> {

    public Map<String, T> items;

    public ObjectList() {
        items = new HashMap<>();
    }

    public T getObject(String key) {
        return items.get(key);
    }

    public Map getItems() {
        return this.items;
    }

}
