package net.ncguy.utils.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

/**
 * Created by Nick on 02/03/2015 at 15:31.
 * Project: Technical Feasibility
 * Package: net.ncguy.utils.file
 */
public class FileUtils {

    public static Stream<String> lines(String path) throws IOException {
        return lines(new File(path));
    }
    public static Stream<String> lines(File file) throws IOException {
        return Files.lines(file.toPath());
    }

}
