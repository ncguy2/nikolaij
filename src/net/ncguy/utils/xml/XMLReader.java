package net.ncguy.utils.xml;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Created by ncguy on 20/12/2014 at 12:38.
 */
public class XMLReader {

    File file;
    boolean isFileValid;

    Document doc;
    NodeList activeNodeList;
    Node currentNode;
    Element currentElement;

    /**
     * Takes the file given and checks if it is valid
     *
     * @param f The file to inspect
     */
    public XMLReader(File f) {
        file = f;
        if(f.exists()) isFileValid = true;
        else isFileValid = false;
    }

    /**
     * Loads the file into memory to be easily readable
     */
    public void init() {
        if(isFileValid) {
            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                doc = db.parse(file);
                doc.getDocumentElement().normalize();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Gets the root element
     *
     * @return the root element
     */
    public Element getDocumentElement() {
        return doc.getDocumentElement();
    }

    /**
     * Gets attribute with the given tag from the root element
     *
     * @param tag the attribute name
     * @return the attribute contents
     */
    public String getDocumentAttribute(String tag) {
        String str = "";
        try{
            str = getDocumentElement().getAttribute(tag);
        }catch (Exception e) {}
        return str;
    }

    /**
     * Gets all attributes of the root element
     *
     * @return list containing all attributes
     */
    public NamedNodeMap getDocumentAttributes() {
        try{
            return getDocumentElement().getAttributes();
        }catch (Exception e) {}
        return null;
    }

    /**
     * Gets all nodes with the tag name provided
     *
     * @param tag the tag to search for
     * @return the NodeList containing all discovered nodes
     */
    public NodeList getNodeList(String tag) {
        NodeList list = doc.getElementsByTagName(tag);
        activeNodeList = list;
        return list;
    }

    /**
     * Gets a single node within the current list
     *
     * @param index the index to get
     * @return the node at the provided index
     */
    public Node getNode(int index) {
        return getNode(activeNodeList, index);
    }

    /**
     * Gets a single node within the provided list
     *
     * @param list the NodeList to search through
     * @param index the index to get
     * @return the node at the provided index (Clamped between 0 and the max value of list)
     */
    public Node getNode(NodeList list, int index) {
        if(index >= list.getLength()) index = list.getLength()-1;
        Node node = list.item(index);
        currentNode = node;
        return node;
    }

    /**
     * Gets the attribute with the supplied name
     *
     * @param tag the attribute name
     * @return the contents of the attribute, empty string if attribute does not exist
     */
    public String getAttributeOfNode(String tag) {
        String str = "";
        try {
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) currentNode;
                str = e.getAttribute(tag);
            }
        }catch(Exception e) {}
        return str;
    }

    public String getAttributeOfElement(String tag, String attr) {
        String str = "";
        try{
            str = currentElement.getElementsByTagName(tag).item(0).getAttributes().getNamedItem(attr).getNodeValue();
        }catch(Exception e) {}
        return str;
    }

    public String getAttributeOfElement(Element element, String tag, String attr) {
        String str = "";
        try{
            str = element.getElementsByTagName(tag).item(0).getAttributes().getNamedItem(attr).getNodeValue();
        }catch(Exception e) {}
        return str;
    }

    /**
     * Gets the attribute with the supplied name from a child of the current node
     *
     * @param childTag the name of the child
     * @param tag the name of the attribute to search for
     * @return
     */
    public String getAttributeOfChild(String childTag, String tag) {
        String str = "";
        try {
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) currentNode;
                str = e.getElementsByTagName(childTag).item(0).getAttributes().getNamedItem(tag).getNodeValue();
            }
        }catch(Exception e) {}
        return str;
    }

    /**
     * Gets all attributes of an element
     *
     * @param tag the element to search through
     * @return list containing all attributes
     */
    public NamedNodeMap getAttributesOfElement(String tag) {
        try {
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) currentNode;
                return e.getAttributes();
            }
        }catch(Exception e) {}
        return null;
    }

    /**
     * Gets all attributes of an element
     *
     * @param childTag the child of the current node to look through
     * @return list containing all attributes of the child
     */
    public NamedNodeMap getAttributesOfChild(String childTag) {
        try {
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) currentNode;
                return e.getElementsByTagName(childTag).item(0).getAttributes();
            }
        }catch(Exception e) {}
        return null;
    }

    /**
     * Gets the node value of an element
     *
     * @param tag the element to search for
     * @return the content of the element provided
     */
    public String getNodeValue(String tag) {
        String str = "";
        try {
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) currentNode;
                str = e.getNodeValue();
            }
        }catch(Exception e) {}
        return str;
    }

    public XMLReader setNodeList(NodeList list) {
        activeNodeList = list;
        return this;
    }

    public String getAttributeOfNode(Node node, String tag) {
        String str = "";
        try{
            str = node.getAttributes().getNamedItem(tag).getNodeValue();
        }catch(Exception e) {}
        return str;
    }

    public XMLReader setElement(Element e) {
        this.currentElement = e;
        return this;
    }
}
