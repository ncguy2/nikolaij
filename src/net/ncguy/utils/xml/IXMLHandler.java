package net.ncguy.utils.xml;

import java.io.File;

/**
 * Created by ncguy on 05/12/2014 at 00:07,
 * Project: Quiz.
 */
public interface IXMLHandler {

    public void init(File f);
    public void prepare();
    public void process();
    public void finish();

}
