package net.ncguy.utils.xml;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

/**
 * Created by ncguy on 05/12/2014 at 00:09,
 * Project: Quiz.
 */
public class BaseXMLWriter implements IXMLHandler {

    public File f;
    public Document doc;

    @Override
    public void init(File f) {
        this.f = f;
    }

    @Override
    public void prepare() {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.newDocument();
            this.doc = doc;
        }catch (Exception e) {}
    }

    @Override
    public void process() {

    }

    @Override
    public void finish() {
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();
            DOMSource src = new DOMSource(doc);
            StreamResult res = new StreamResult(f);

            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            t.transform(src, res);

        }catch (Exception e) {}
    }
}
