package net.ncguy.utils.xml;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ncguy on 04/12/2014 at 23:41,
 * Project: Quiz.
 */
public class BaseXMLReader implements IXMLHandler {

    public File f;

    @Override
    public void init(File f) {
        this.f = f;
    }

    @Override
    public void prepare() {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(f);
            doc.getDocumentElement().normalize();
        }catch(Exception e) {}
    }

    @Override
    public void process() {}

    @Override
    public void finish() {

    }

}
