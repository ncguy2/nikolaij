package net.ncguy.utils.general;

import java.io.File;

/**
 * Created by ncguy on 25/12/2014 at 16:52,
 * Project: Development.
 */
public class JavaUtils {

    public static double getJavaVersion() {
        return Double.parseDouble(System.getProperty("java.specification.version"));
    }

    public static String getJavaPath() {
        return System.getProperty("java.home");
    }

    public static String getFileName(String path) {
        File f = new File(path);
        return f.getName();
    }

}
