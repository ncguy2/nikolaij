package net.ncguy.utils.general;

import java.io.File;

/**
 * Created by ncguy on 21/12/2014 at 15:04,
 * Project: Development.
 */
public class TextUtils {

    public static void printLine(int length) {
        for(int i = 0; i < length; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    public static String capitalize(String line) {
        return Character.toUpperCase(line.charAt(0))+line.substring(1);
    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    public static String getFileName(File file) {
        return getFileName(file, true);
    }
    public static String getFileName(File file, boolean ext) {
        String str = file.getName();
        if(!ext){
            int i = str.lastIndexOf('.');
            str = str.substring(0, i);
        }
        return str;
    }

    public static String getFileExtention(File file) {
        return getFileExtention(file, false);
    }

    public static String getFileExtention(File file, boolean dot) {
        String str = file.getName();
        System.out.println("File: "+str);
        int i = str.lastIndexOf('.');
        if(!dot) i++;
        str = str.substring(i, str.length());
        System.out.println("Ext: "+str);
        return str;
    }

}
