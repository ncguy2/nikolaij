package net.ncguy.utils.general;

import java.util.Random;

/**
 * Created by ncguy on 05/12/2014 at 00:20,
 * Project: Quiz.
 */
public class MathUtils {

    static Random rand = new Random();

    /**
     * Returns a pseudo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimum Value
     * @param max Maximum Value, Must be Greater than Min
     * @return Integer between min and max, inclusive
     * @see java.util.Random#nextInt(int)
     */
    public static int randInt(int min, int max) {
        if(max <= min) max = min+10;
        return rand.nextInt((max-min)+1)+min;
    }

    public static int clamp(int val, int min, int max) {
        return Math.max(min, Math.min(max, val));
    }
}
