package net.ncguy.utils.stream;

import com.sun.istack.internal.NotNull;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Nick on 02/02/2015 at 13:56.
 * Project: Development
 * Package: net.ncguy.utils.stream
 */
public class MultiOutputStream extends OutputStream {

    OutputStream[] streams;

    public MultiOutputStream(OutputStream... streams){
        this.streams = streams;
    }

    @Override
    public void write(int b) throws IOException {
        for(OutputStream stream : streams)
            stream.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        for(OutputStream stream : streams)
            stream.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        for(OutputStream stream : streams)
            stream.write(b, off, len);
    }

    @Override
    public void flush() throws IOException {
        for(OutputStream stream : streams)
            stream.flush();
    }

    @Override
    public void close() throws IOException {
        for(OutputStream stream : streams)
            stream.close();
    }
}
